from datetime import date
from src.schemas.base import BaseSchema
from decimal import Decimal





class StaticSchema(BaseSchema):
    id: int
    date: date
    views: int
    clicks: int
    cost: Decimal   
    cpc: Decimal | None = 0
    cpm: Decimal | None = 0



class StaticCreateSchema(BaseSchema):
    date: date
    views: int | None = 0
    clicks: int | None = 0
    cost: Decimal | None = 0



class StaticUpdateSchema(BaseSchema):
    id: int



