from src.db.models.static import Static
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, delete, update
from fastapi import HTTPException, status
from src.schemas.static import StaticCreateSchema
from datetime import date





async def retrive_static(id: int, session: AsyncSession) -> Static:
    static: Static = await session.scalar(select(Static).where(Static.id == id))

    if static is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={
                'code': status.HTTP_404_NOT_FOUND,
                'description': f'Static with id: {id} is not found :('
            }
        )
    return static



async def create_static(static: StaticCreateSchema, session: AsyncSession) -> Static:
    static_db = Static(**static.dict())
    try:
        static_db.cpc = static_db.cost / static_db.clicks
        static_db.cpm = static_db.cost / static_db.views * 1000
    except Exception as ex:
        pass
    session.add(static_db)
    await session.commit()
    return static_db



async def delete_static(id: int, session: AsyncSession) -> None:
    static: Static = await session.scalar(select(Static).where(Static.id == id))

    if static is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={
                "code": status.HTTP_404_NOT_FOUND,
                "description": f"Static with id: {id} not found",
            },
        )

    await session.execute(delete(Static).where(Static.id == id))

    await session.commit()


async def update_static(id:int, static: StaticCreateSchema, session: AsyncSession) -> dict:
    static_db: Static = await session.scalar(select(Static).where(Static.id == id))

    if static_db is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail={
                "code": status.HTTP_404_NOT_FOUND,
                "description": f"Static with id {id} not found",
            },
        )
    static_payload = static.dict()
    static_payload.setdefault('id', id)
    await session.execute(update(Static).values(**static_payload).where(Static.id == id))
    await session.commit()
    return {**static.dict(), 'id': id}

