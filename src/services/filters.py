

def get_static_all_filter(query, Static, data):
    if data.get('views') is not None:
        query = query.where(Static.views == data.get('views'))
    if data.get('clicks') is not None:
        query = query.where(Static.clicks == data.get('clicks'))
    if data.get('cost') is not None:
        query = query.where(Static.cost == data.get('cost'))
    if data.get('from_date') is not None:
        query = query.where(Static.date >= data.get('from_date'))
    if data.get('to') is not None:
        query = query.where(Static.date <= data.get('to'))
    return query