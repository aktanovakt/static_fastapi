from fastapi import APIRouter, Depends
from src.schemas.static import StaticSchema, StaticCreateSchema
from sqlalchemy.ext.asyncio import AsyncSession
from src.db.dependencies import get_async_session
from src.db.models.static import Static
from src.services.static import retrive_static, create_static, delete_static, update_static
from sqlalchemy import select
from fastapi.responses import Response
from typing import Optional
from datetime import date
from decimal import Decimal
from sqlalchemy import delete
from src.services.filters import get_static_all_filter


static_router = APIRouter(
    tags=['static'],
    prefix='/static'
)



@static_router.get('/static/{id}', response_model=StaticSchema)
async def get_static_api(id: int, session: AsyncSession = Depends(get_async_session)) -> Static:
    static: Static = await retrive_static(id=id, session=session)
    return static


@static_router.post('/static/', response_model=StaticSchema)
async def create_static_api(static: StaticCreateSchema, session: AsyncSession = Depends(get_async_session)) -> Static:
    return await create_static(static=static, session=session)


@static_router.get('/static_all/', response_model=list[StaticSchema])
async def get_all_static_api(
    views: Optional[int] = None,
    clicks: Optional[int] = None,
    cost: Optional[Decimal] = None,
    from_date: Optional[date] = None,
    to: Optional[date] = None,
    session: AsyncSession = Depends(get_async_session)
) -> list[StaticSchema]:
    
    query = select(Static)
    data = {
        'views': views, 'clicks': clicks, 'cost': cost, 'from_date': from_date, 'to': to
    }
    query = get_static_all_filter(query=query, Static=Static, data=data)
    static: list[Static] = (await session.scalars(query)).all()
    return [StaticSchema.from_orm(s) for s in static]


@static_router.delete('/static/{id}')
async def delete_static_api(id: int, session: AsyncSession = Depends(get_async_session)) -> Response:
    await delete_static(id=id, session=session)
    return Response('Seccess')

@static_router.delete('/static/')
async def delete_static_all(session: AsyncSession = Depends(get_async_session)) -> Response:
    await session.execute(delete(Static))
    await session.commit()
    return Response('Seccess')



@static_router.put('/static/{id}')
async def update_static_api(id: int, static: StaticCreateSchema, session: AsyncSession = Depends(get_async_session)):
    return await update_static(id=id, static=static, session=session)