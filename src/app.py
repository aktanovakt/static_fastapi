from fastapi import FastAPI, status
from fastapi.responses import Response
from src.routers.static import static_router



def create_app() -> FastAPI:
    app = FastAPI(title='Static Service API')


    @app.get('/status_check')
    async def status_check() -> Response:
        return Response(status_code=status.HTTP_200_OK)
    
    app.include_router(static_router)
    return app