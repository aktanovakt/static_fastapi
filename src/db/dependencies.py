from sqlalchemy.ext.asyncio import AsyncSession
from typing import AsyncGenerator
from src.db.base import async_session


async def get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session