from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from src.config.setting import db_settings
from sqlalchemy.orm import declarative_base, sessionmaker

async_engine = create_async_engine(
    db_settings.url,
    future=True,
    pool_size=20,
    pool_pre_ping=True,
    pool_use_lifo=True,
    echo=db_settings.echo,
)

async_session = sessionmaker(
    future=True,
    class_=AsyncSession,
    bind=async_engine,
    expire_on_commit=False,
)

Base = declarative_base()

