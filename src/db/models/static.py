from sqlalchemy import Integer, DECIMAL, Column, Date
from sqlalchemy.orm import Mapped
from src.db.base import Base
from datetime import date
from decimal import Decimal


class Static(Base):
    __tablename__ = "static"
    
    id: Mapped[int] = Column(Integer, primary_key=True, index=True, unique=True)
    date: Mapped[date] = Column(Date, default=date.today())
    views: Mapped[int] = Column(Integer)
    clicks: Mapped[int] = Column(Integer)
    cost: Mapped[Decimal] = Column(DECIMAL(10, 2))
    cpc: Mapped[Decimal] = Column(DECIMAL(10, 2))
    cpm: Mapped[Decimal] = Column(DECIMAL(10, 2))
