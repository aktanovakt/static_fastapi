from pydantic import BaseSettings



class DatabaseSettings(BaseSettings):
    driver: str = "postgresql+asyncpg"
    db: str
    user: str
    password: str
    host: str
    port: str
    echo: bool = False

    class Config:
        env_file = ".env"
        env_prefix = "postgres_"

    @property
    def url(self) -> str:
        return f"{self.driver}://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"

db_settings = DatabaseSettings()


# print(db_settings.url)


class TestDatabaseSettings(DatabaseSettings):
    class Config:
        env_prefix = "test_postgres_"

    @property
    def url(self) -> str:
        return f"{self.driver}://{self.user}:{self.password}@{self.host}:{self.port}/{self.db}"


test_database_settings = TestDatabaseSettings()