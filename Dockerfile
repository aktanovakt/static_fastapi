FROM python:3.10-slim as builder

WORKDIR /app
RUN apt-get update && apt-get install -y curl
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/etc/poetry python3 -
ENV PATH=$PATH:/etc/poetry/bin

COPY ./pyproject.toml ./poetry.lock ./
RUN poetry config virtualenvs.in-project true
RUN poetry install --only main
RUN poetry add uvloop

FROM python:3.10-slim
ENV PATH=/app/.venv/bin:$PATH \
    PYTHONPATH=$PYTHONPATH:/app/src

WORKDIR /app

COPY migrations migrations
COPY src src
COPY alembic.ini ./
COPY main.py ./
COPY --from=builder /app/.venv .venv

ENTRYPOINT ["uvicorn", "src.app:create_app", "--factory", "--loop", "uvloop", "--host", "0.0.0.0", "--port", "8080"]
