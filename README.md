## SciPlay Static Service


### Запкуск проекта через docker:
```
sudo docker-compose up --build
```


### Запуск без docker:
##### Переименуйте .env.example на .env !!
```
poetry init
poetry shell
poetry update
poetry run uvicorn app:create_app --reload --port 8080
```


### Команда для создании миграции после изменения моделей, сохроняются в формате YYYY-MM-DD_name:
```
alembic revision --autogenerate -m "name"
alembic upgrade head
```

### Команда для обновления бд:
```
alembic upgrade head
```

### Откат бд на 1 миграцию:
```
alembic downgrade -1
```



## Структура проекта:
```
project
├── migrations/ - Миграции
├── src - Главная work папка
│    ├── config/
│    │       └── setting.py - Получение зависимостей
│    │
│    │──  db/
│    │       ├── models/
│    │       │       └── static.py - Создания модели Static со всеми полями
│    │       │           
│    │       ├── base.py - Настройка подключения к бд
│    │       └── dependencies.py - Ассинхронная сессия в бд
│    │
│    │── routers/
│    │       └── static.py - Url для взоимодействия с API
│    │
│    │── schemas
│    │       ├── base.py - Настройка основы схем
│    │       └── static.py - Схемы для валидации
│    │
│    │── services
│    │       ├── filters.py - Фильтры
│    │       └── static.py - "Бизнес логика"
│    │  
│    └── app.py - Сборка API
│   
│
│── tests/
│      ├── api/
│      │      └── test_status.py
│      │ 
│      ├── db_test.py
│      └── test.py
│
├── .env.example
├── .alembic.ini
├── docker-compose.yml
├── Dockerfile
├── main.py - Запуск
├── poetry.lock
├── pyproject.toml
└── README.md
```


## Документация
```
http://localhost:8080/docs
```



## Запросы
```
GET [/static/static/{id}] - получение одного объекта по id, сразу высчитывает cpc и cpm
PUT [/static/static/{id}] - обновление объекта по id
DELETE [/static/static/{id}] - удаление одного объекста по id
POST [/static/static/] - создание объекта
DELETE [/static/static/] - УДАЛЯЕТ ВСЕ ОБЪЕКТЫ !!!
GET [/static/static_all/] - получения всех объектов, с возможностью фильтрации по дате
```

