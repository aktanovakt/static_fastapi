from typing import Any, Coroutine

import httpx
import pytest


@pytest.mark.asyncio
async def test_statuscheck(http_client: Coroutine[Any, Any, httpx.AsyncClient]) -> None:
    client = await http_client
    response = await client.get("/statuscheck")
    assert response.status_code == 200
