from typing import AsyncGenerator, AsyncIterable

import pytest
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine

from src.config.setting import test_database_settings
from src.db.base import Base, async_session




@pytest.fixture(scope="session")
def engine() -> AsyncEngine:
    return create_async_engine(
        test_database_settings.url,
        future=True,
        pool_use_lifo=True,
        pool_pre_ping=True,
        pool_size=20,
    )


@pytest.fixture(autouse=True)
async def session(engine: AsyncEngine) -> AsyncGenerator[AsyncSession, None]:
    async with engine.connect() as conn:
        transaction = await conn.begin()
        async_session.configure(bind=conn)

        async with async_session() as session:
            yield session

        if transaction.is_active:
            await transaction.rollback()


@pytest.fixture(scope="session", autouse=True)
async def setup_database(engine: AsyncEngine) -> AsyncIterable[None]:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
