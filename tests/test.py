import asyncio
from asyncio import AbstractEventLoop
from typing import Iterable

import dotenv
import httpx
import pytest
from fastapi import FastAPI

dotenv.load_dotenv(".env")

pytest_plugins = [
    "tests.conftest_db",
]


@pytest.fixture(scope="session")
def fastapi_app() -> FastAPI:
    from src.app import create_app

    return create_app()


@pytest.fixture(scope="session")
async def http_client(fastapi_app: FastAPI):
    return httpx.AsyncClient(
        app=fastapi_app,
        base_url="http://test",
    )


@pytest.fixture(scope="session")
def event_loop() -> Iterable[AbstractEventLoop]:
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()
